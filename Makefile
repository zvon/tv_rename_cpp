CC ?= clang
CXX ?= clang++
CFLAGS ?= -O2 -Wall -Wextra -std=c++11
PREFIX ?= /usr/local/bin
APPDIR ?= /usr/share/applications
ICONDIR ?= /usr/share/icons/hicolor
LOCALES = locale/cs/LC_MESSAGES/tv_rename.mo\
		  locale/en_US/LC_MESSAGES/tv_rename.mo
LDFLAGS = -lcurl -lsqlite3

GTKFLAGS = `pkg-config gtkmm-3.0 --cflags`
GTKLIBS = `pkg-config gtkmm-3.0 --libs`

ifneq ($(OS),Windows_NT)
	UNAME_S := $(shell uname -s)
	ifeq ($(UNAME_S),Darwin)
		LDFLAGS += -lintl
		GTKFLAGS +=  `pkg-config gtk-mac-integration-gtk3 --cflags`
		GTKLIBS +=  `pkg-config gtk-mac-integration-gtk3 --libs`
	endif
endif

.PHONY: default
default: tv_rename

.PHONY: clean
clean:
	rm -Rf *.o tv_rename tv_rename_gui locale *.gcda *.gcno *.gcov\
		   gcovr_report.xml

.PHONY: install
install: tv_rename
	install -d $(PREFIX)/
	install -m 755 tv_rename $(PREFIX)/
	for f in locale/* ; do install -m 644 "$${f}/LC_MESSAGES/tv_rename.mo"\
		                                 "/usr/share/$${f}/LC_MESSAGES/"\
										 ; done

.PHONY: install_gui
install_gui: gui
	install -d $(PREFIX)/
	install -m 755 tv_rename_gui $(PREFIX)/
	install -d $(APPDIR)
	install -m 755 tv_rename_gui.desktop $(APPDIR)/
	install -d $(ICONDIR)
	install -m 644 tv_rename.svg $(ICONDIR)/scalable/apps/
	gtk-update-icon-cache -f $(ICONDIR)
	for f in locale/* ; do install -m 644 "$${f}/LC_MESSAGES/tv_rename.mo"\
		                                  "/usr/share/$${f}/LC_MESSAGES/"\
										  ; done

.PHONY: uninstall
uninstall:
	rm $(PREFIX)/tv_rename

.PHONY: uninstall_gui
uninstall_gui:
	rm $(PREFIX)/tv_rename_gui
	rm $(APPDIR)/tv_rename_gui.desktop
	rm $(ICONDIR)/scalable/apps/tv_rename.svg
	gtk-update-icon-cache -f $(ICONDIR)

tv_rename: functions.o filesystem.o network.o tv_rename.o progress.o main.o\
	       $(LOCALES)
	$(CXX) $(CFLAGS) -o tv_rename main.o tv_rename.o functions.o\
						filesystem.o network.o progress.o $(LDFLAGS)
	strip tv_rename

filesystem.o: unix/filesystem.cpp
	$(CXX) $(CFLAGS) -c unix/filesystem.cpp -o filesystem.o
functions.o: functions.cpp
	$(CXX) $(CFLAGS) -c functions.cpp
network.o: unix/network.cpp
	$(CXX) $(CFLAGS) -c unix/network.cpp
tv_rename.o: tv_rename.cpp
	$(CXX) $(CFLAGS) -c tv_rename.cpp
progress.o: progress.cpp
	$(CXX) $(CFLAGS) -c progress.cpp
main.o: main.cpp
	$(CXX) $(CFLAGS) -c main.cpp

.PHONY: gui
gui: tv_rename_gui

GUIOBJECTS = gui.o mainwindow.o seasonwindow.o databasewindow.o\
	         searchwindow.o gtkfunctions.o network.o functions_gui.o\
	         filesystem_u_gui.o tv_rename_gui.o progress_gui.o\
	         progresswindow.o

tv_rename_gui: $(GUIOBJECTS) $(LOCALES)
	$(CXX) $(CFLAGS) -o $@ $(GUIOBJECTS) $(GTKFLAGS) $(GTKLIBS) $(LDFLAGS)
	strip tv_rename_gui

filesystem_u_gui.o: unix/filesystem.cpp
	$(CXX) $(CFLAGS) -c $^ -o filesystem_u_gui.o -DGUI
functions_gui.o: functions.cpp
	$(CXX) $(CFLAGS) -c $^ -o functions_gui.o -DGUI
tv_rename_gui.o: tv_rename.cpp
	$(CXX) $(CFLAGS) -c $^ -o tv_rename_gui.o -DGUI
gui.o: gtk/gui.cpp
	$(CXX) $(CFLAGS) -o $@ -c $^ $(GTKFLAGS) -DGUI
mainwindow.o: gtk/mainwindow.cpp
	$(CXX) $(CFLAGS) -o $@ -c $^ $(GTKFLAGS) -DGUI
seasonwindow.o: gtk/seasonwindow.cpp
	$(CXX) $(CFLAGS) -o $@ -c $^ $(GTKFLAGS) -DGUI
databasewindow.o: gtk/databasewindow.cpp
	$(CXX) $(CFLAGS) -o $@ -c $^ $(GTKFLAGS) -DGUI
searchwindow.o: gtk/searchwindow.cpp
	$(CXX) $(CFLAGS) -o $@ -c $^ $(GTKFLAGS) -DGUI
gtkfunctions.o: gtk/gtkfunctions.cpp
	$(CXX) $(CFLAGS) -o $@ -c $^ $(GTKFLAGS) -DGUI
progress_gui.o: progress.cpp
	$(CXX) $(CFLAGS) -o $@ -c $^ $(GTKFLAGS) -DGUI
progresswindow.o: gtk/progresswindow.cpp
	$(CXX) $(CFLAGS) -o $@ -c $^ $(GTKFLAGS) -DGUI

.PHONY: windows
windows: tv_rename.exe

WINDOWS_CLI_OBJECTS = tv_rename_cli.obj functions_cli.obj filesystem_cli.obj\
					  network_cli.obj progress_cli.obj sqlite3.obj main.obj

sqlite3.obj: sqlite-amalgamation/sqlite3.c
	$(CXX) -MD -EHsc -Fo"$@" $^ -D_WIN32 -DUNICODE -c
tv_rename_cli.obj: tv_rename.cpp
	$(CXX) -MD -EHsc -Fo"$@" $^ -D_WIN32 -DUNICODE -c
functions_cli.obj: functions.cpp
	$(CXX) -MD -EHsc -Fo"$@" $^ -D_WIN32 -DUNICODE -c
filesystem_cli.obj: windows/filesystem.cpp
	$(CXX) -MD -EHsc -Fo"$@" $^ -D_WIN32 -DUNICODE -c
network_cli.obj: windows/network.cpp
	$(CXX) -MD -EHsc -Fo"$@" $^ -D_WIN32 -DUNICODE -c
progress_cli.obj: progress.cpp
	$(CXX) -MD -EHsc -Fo"$@" $^ -D_WIN32 -DUNICODE -c
main.obj: main.cpp
	$(CXX) -MD -EHsc -Fo"$@" $^ -D_WIN32 -DUNICODE -c

tv_rename.exe: $(WINDOWS_CLI_OBJECTS) tv_rename_stringtable.res
	$(CXX) -MD -EHsc -Fe"tv_rename" $(WINDOWS_CLI_OBJECTS)\
		   -D_WIN32 -DUNICODE -link wininet.lib shlwapi.lib ole32.lib\
		   shell32.lib user32.lib tv_rename_stringtable.res

.PHONY: windows_gui
windows_gui: tv_rename_gui.exe

WINDOWS_GUI_OBJECTS =  gui.obj mainwindow.obj\
	                   gui_functions.obj tv_rename.obj\
					   filesystem.obj functions.obj network.obj\
			           progress.obj sqlite3.obj patternwindow.obj\
					   progresswindow.obj seasonwindow.obj\
					   databasewindow.obj searchwindow.obj

gui.obj: win32/gui.cpp
	$(CXX) -MD -EHsc -Fo"$@" $^ -DGUI -D_WIN32 -DUNICODE -c
mainwindow.obj: win32/mainwindow.cpp
	$(CXX) -MD -EHsc -Fo"$@" $^ -DGUI -D_WIN32 -DUNICODE -c
gui_functions.obj: win32/gui_functions.cpp
	$(CXX) -MD -EHsc -Fo"$@" $^ -DGUI -D_WIN32 -DUNICODE -c
filesystem.obj: windows/filesystem.cpp
	$(CXX) -MD -EHsc -Fo"$@" $^ -DGUI -D_WIN32 -DUNICODE -c
network.obj: windows/network.cpp
	$(CXX) -MD -EHsc -Fo"$@" $^ -DGUI -D_WIN32 -DUNICODE -c
patternwindow.obj: win32/patternwindow.cpp
	$(CXX) -MD -EHsc -Fo"$@" $^ -DGUI -D_WIN32 -DUNICODE -c
progresswindow.obj: win32/progresswindow.cpp
	$(CXX) -MD -EHsc -Fo"$@" $^ -DGUI -D_WIN32 -DUNICODE -c
seasonwindow.obj: win32/seasonwindow.cpp
	$(CXX) -MD -EHsc -Fo"$@" $^ -DGUI -D_WIN32 -DUNICODE -c
databasewindow.obj: win32/databasewindow.cpp
	$(CXX) -MD -EHsc -Fo"$@" $^ -DGUI -D_WIN32 -DUNICODE -c
searchwindow.obj: win32/searchwindow.cpp
	$(CXX) -MD -EHsc -Fo"$@" $^ -DGUI -D_WIN32 -DUNICODE -c
tv_rename.obj: tv_rename.cpp
	$(CXX) -MD -EHsc -Fo"$@" $^ -DGUI -D_WIN32 -DUNICODE -c
functions.obj: functions.cpp
	$(CXX) -MD -EHsc -Fo"$@" $^ -DGUI -D_WIN32 -DUNICODE -c
progress.obj: progress.cpp
	$(CXX) -MD -EHsc -Fo"$@" $^ -DGUI -D_WIN32 -DUNICODE -c

tv_rename_gui.exe: $(WINDOWS_GUI_OBJECTS) tv_rename.res tv_rename_stringtable.res
	$(CXX) -MD -EHsc -Fe"tv_rename_gui" $(WINDOWS_GUI_OBJECTS) -DGUI -D_WIN32 -DUNICODE\
		   -link wininet.lib shlwapi.lib ole32.lib\
		   shell32.lib user32.lib gdi32.lib comctl32.lib tv_rename.res tv_rename_stringtable.res
	mt -manifest tv_rename_gui.exe.manifest -outputresource:tv_rename_gui.exe

tv_rename.res: tv_rename.rc
	rc $^

tv_rename_stringtable.res: tv_rename_stringtable.rc
	rc $^

locale/%/LC_MESSAGES:
	mkdir -p $@

locale/%/LC_MESSAGES/tv_rename.mo: translations/%.po locale/%/LC_MESSAGES
	msgfmt -c $< -o $<.mo
	mv $<.mo $@

test.out: tests/test.cpp functions.cpp unix/network.cpp progress.cpp\
	   unix/filesystem.cpp tv_rename.cpp
	$(CXX) $(CFLAGS) $^ $(LDFLAGS) --coverage -g -fsanitize=address,undefined -o test.out

.PHONY: check
check: test.out
	sudo ./test.out
