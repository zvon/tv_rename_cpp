#include <string>
#include <vector>
#include <windows.h>

class SearchWindow {
public:
    SearchWindow(
        HINSTANCE hInstance,
        const std::vector< std::pair< std::wstring, std::wstring > > &languages,
        const int lang_pos, const wchar_t *show, HWND parent_window );

    void mainLoop();
    static LRESULT CALLBACK messageHandler( HWND hwnd, UINT umsg, WPARAM wParam,
                                            LPARAM lParam );
    bool accepted();
    std::wstring getShow();
    std::wstring getShowID();
    int getLangID();
    ~SearchWindow() {
        EnableWindow( parent, true );
        SetFocus( parent );
    }

private:
    HWND window;
    HWND show_input;
    HWND lang_input;
    HWND possible_label;
    HWND possible_input;
    HWND parent;

    const int window_width{ 450 };
    const int window_height{ 200 };
    const std::vector< std::pair< std::wstring, std::wstring > > &langs;
    std::vector< std::pair< std::wstring, std::wstring > > possible_shows;
    int selected{ -1 };
    int lang_id{ -1 };

    void process();
    void ok();
    void cancel();

    static SearchWindow *sw;
};
