#include <windows.h>

#include "../tv_rename.hpp"

class MainWindow {
public:
    MainWindow( HINSTANCE hInstance, int nCmdShow );

    void mainLoop();
    static LRESULT CALLBACK messageHandler( HWND hwnd, UINT umsg, WPARAM wParam,
                                            LPARAM lParam );

private:
    int langPos( const std::wstring &lang );
    void readDefaultPattern( const std::wstring &base_dir );
    void process();
    void rename();
    void dbAdd();
    static void dbUpdate();
    static void dbRefresh();
    static void dbClean();
    static void dbManage();
    static void dbPattern();
    static void patternHelp();

    HFONT hFont;
    HWND window;
    HINSTANCE hInst;
    const int window_width{ 450 };
    const int window_height{ 350 };
    std::vector< std::pair< string, string > > languages;
    std::vector< std::pair< string, string > > possible_shows;
    std::wstring default_pattern{};
    std::vector< int > selected;
    std::map< int, std::map< int, string > > files;

    HWND show_input, language_input, dir_input, pattern_input, trust_input,
        dvd_input;
    HWND possible_label, possible_input, rename_button, db_add_button;

    static MainWindow *mw;
};
