#ifndef GTKMM_SEASON_WINDOW
#define GTKMM_SEASON_WINDOW

#include <gtkmm/button.h>
#include <gtkmm/checkbutton.h>
#include <gtkmm/label.h>
#include <gtkmm/layout.h>
#include <gtkmm/window.h>

class SeasonWindow : public Gtk::Window {
public:
    SeasonWindow( const std::vector< int > &seasons,
                  std::vector< int > &_returned );
    virtual ~SeasonWindow() = default;

private:
    void confirm();
    void select_all();
    void select_none();

protected:
    Gtk::Button m_confirm;
    Gtk::Button m_all;
    Gtk::Button m_none;

    Gtk::Label m_label;

    // TODO maybe use grid
    Gtk::Layout m_layout;

    std::vector< Gtk::CheckButton > m_checks;

    std::vector< int > &returned;
};

#endif
