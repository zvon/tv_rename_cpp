#include "seasonwindow.hpp"
#include <string>

#include "../functions.hpp"
#include "../resources_linux.h"

void SeasonWindow::confirm() {
    // go through all checkbuttons and save numbers
    // of checked boxes into returned vector
    // then quit
    for ( auto &x : m_checks ) {
        if ( x.get_active() ) {
            returned.push_back( std::stoi( x.get_label() ) );
        }
    }
    hide();
}

void SeasonWindow::select_all() {
    // set all check boxes to checked
    for ( auto &x : m_checks ) {
        x.set_active( true );
    }
}

void SeasonWindow::select_none() {
    // set all check boxes to unchecked
    for ( auto &x : m_checks ) {
        x.set_active( false );
    }
}

SeasonWindow::SeasonWindow( const std::vector< int > &seasons,
                            std::vector< int > &_returned )
        : returned( _returned ) {
    set_title( _( GUI_WINDOW_SEASON ) );

    set_default_size( 250, 250 );

    add( m_layout );

    size_t x{ 5 }, y{ 25 };

    // create a check box for each season
    for ( auto &s : seasons ) {
        m_checks.emplace_back( std::to_string( s ) );
        m_layout.put( m_checks.back(), x, y );

        if ( x == 185 ) {
            x = 5;
            y += 20;
        } else {
            x += 60;
        }
    }

    m_layout.put( m_label, 5, 5 );
    m_label.set_label( _( SELECT_SEASONS ) );

    m_layout.put( m_confirm, 165, 215 );
    m_layout.put( m_all, 130, 175 );
    m_layout.put( m_none, 5, 175 );

    m_confirm.set_size_request( 80, 30 );
    m_all.set_size_request( 80, 30 );
    m_none.set_size_request( 80, 30 );

    m_confirm.set_label( _( SELECT ) );
    m_all.set_label( _( SELECT_ALL ) );
    m_none.set_label( _( SELECT_NONE ) );

    m_confirm.signal_clicked().connect(
        sigc::mem_fun( *this, &SeasonWindow::confirm ) );
    m_all.signal_clicked().connect(
        sigc::mem_fun( *this, &SeasonWindow::select_all ) );
    m_none.signal_clicked().connect(
        sigc::mem_fun( *this, &SeasonWindow::select_none ) );

    property_modal().set_value( true );

    show_all_children();
}
