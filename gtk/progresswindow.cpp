#include "progresswindow.hpp"
#include <gtkmm/liststore.h>
#include <mutex>

#include "../functions.hpp"
#include "../resources_linux.h"

ProgressWindow::ProgressWindow() {
    set_title( _( GUI_WINDOW_PROGRESS ) );

    set_default_size( 500, 55 );

    add( *layout );
    set_size_request( 500, 55 );
    set_resizable( false );

    pb->set_size_request( 490, 5 );
    label->set_size_request( 500, 25 );

    // set widgets' location
    layout->put( *pb, 5, 35 );
    layout->put( *label, 5, 5 );

    // show everything
    layout->show();
    layout->show_all_children();

    label->set_text( "TEST" );
}
