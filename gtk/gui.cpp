#include <errno.h>
#ifndef __APPLE__
#include <error.h>
#endif
#include <gtkmm/application.h>
#include <libintl.h>
#include <locale.h>

#include "../filesystem.hpp"
#include "../functions.hpp"
#include "../resources_linux.h"
#include "../tv_rename.hpp"
#include "mainwindow.hpp"

#define API_KEY "42B66F5E-C6BF-423F-ADF9-CC97163472F6"
#define LANGDOMAIN "tv_rename"

int main( int argc, char **argv ) {
    auto app = Gtk::Application::create(
        argc, argv, "org.idonthaveanorganization.tvrename" );

    if ( setlocale( LC_ALL, "" ) == NULL )
        error( 1, errno, "%s", _( INVALID_LOCALE ).c_str() );
    if ( FSLib::exists( "/usr/share/locale/en_US/LC_MESSAGES/tv_rename.mo" ) ) {
        if ( bindtextdomain( LANGDOMAIN, "/usr/share/locale" ) == NULL )
            error( 1, errno, "%s", _( MEM_ALLOC_FAILED ).c_str() );
        if ( textdomain( LANGDOMAIN ) == NULL )
            error( 1, errno, "%s", _( MEM_ALLOC_FAILED ).c_str() );
    } else {
        if ( bindtextdomain( LANGDOMAIN, "./locale" ) == NULL )
            error( 1, errno, "%s", _( MEM_ALLOC_FAILED ).c_str() );
        if ( textdomain( LANGDOMAIN ) == NULL )
            error( 1, errno, "%s", _( MEM_ALLOC_FAILED ).c_str() );
    }

    // TODO let user know that program is authenticating and will start shortly
    authenticate( API_KEY );
    MainWindow mw( app );
    return app->run( mw );
}
