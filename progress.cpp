#include "progress.hpp"

#ifndef GUI

#include <iostream>

#endif

#ifdef _WIN32

#include <conio.h>
#include <windows.h>
#include <winuser.h>

#define cout std::wcout

#else // UNIX

#ifdef GUI
#include "gtk/progresswindow.hpp"
#endif // GUI

#include <sys/ioctl.h>

#define cout std::cout
#define TEXT( a ) a

#endif // UNIX

#ifndef GUI // Don't need terminal specific functions in GUI

#ifdef _WIN32
int getTermWidth() {
    CONSOLE_SCREEN_BUFFER_INFO csbi;

    GetConsoleScreenBufferInfo( GetStdHandle( STD_OUTPUT_HANDLE ), &csbi );
    return csbi.srWindow.Right - csbi.srWindow.Left + 1;
}
#else  // UNIX
int getTermWidth() {
    struct winsize w;
    ioctl( 0, TIOCGWINSZ, &w );

    return w.ws_col;
}
#endif // UNIX

#ifdef _WIN32
void home( size_t /*UNUSED*/ ) {
    static HANDLE h = NULL;
    while ( !h )
        h = GetStdHandle( STD_OUTPUT_HANDLE );
    CONSOLE_SCREEN_BUFFER_INFO info;
    GetConsoleScreenBufferInfo( h, &info );
    COORD c = { 0, info.dwCursorPosition.Y - 1 };
    SetConsoleCursorPosition( h, c );
}
#else  // UNIX
void home( size_t width ) {
    cout << "\x1b[" << width << "D";
}
#endif // UNIX

#endif // not GUI

int getNum( int width, int perc ) {
    return ( width * perc ) / 100;
}

#ifndef GUI
void ProgressBar::print( int perc ) {
    auto width = getTermWidth();
    home( width );
    auto count = getNum( width - 7, perc );
    if ( count < 0 )
        count = 0;
    cout << "[" << string( count, TEXT( '=' ) ).c_str();
    if ( perc != 100 ) {
        int wc = width - 8 - count;
        if ( wc < 0 )
            wc = 0;
        cout << ">" << string( wc, TEXT( ' ' ) ).c_str();
    }
    cout << "] ";
    if ( perc / 10 == 0 )
        cout << "  ";
    else if ( perc / 100 == 0 )
        cout << " ";
    cout << perc << "%" << std::flush;
}

void ProgressBar::print( const string &t ) {
    cout << t << std::endl;
#ifdef _WIN32
    cout << std::endl;
#endif
}

#else // GUI

void ProgressBar::print( int perc ) {
#ifndef _WIN32
    static_cast< ProgressWindow * >( ptr )->setPerc( perc );
#else
    SendMessage( ( HWND )ptr, WM_APP, PROGRESS_PERC, perc );
#endif
}

void ProgressBar::print( const string &t ) {
#ifndef _WIN32
    static_cast< ProgressWindow * >( ptr )->setLabel( t );
#else
    SendMessage( ( HWND )ptr, WM_APP, PROGRESS_STRING, ( LPARAM )t.c_str() );
#endif
}

ProgressBar::~ProgressBar() {
#ifndef _WIN32
    static_cast< ProgressWindow * >( ptr )->hide();
#else
    SendMessage( ( HWND )ptr, WM_CLOSE, 0, 0 );
#endif
}

#endif // GUI
